#AWS Example Configuration

Aws Example Configuration

Main goal 
1. Install instances with database, nginx, drupal, php
2. Success PCI DSS configuration test
3. Using automatization tools and aws cloud

##Getting Started
These instructions help you with create VPC and instances in your aws cloud with automatization tool ansible

## Password For These Example Configuration
Vault files
```
challenge 
```

##Prerequisites
Installed python (tested on python3.5)
Installed pip for python
Pip packages - awscli, boto, boto3, ansible==2.2.1.0, 


##Info
In production change all passwords for vault files, deployment.yml, preferences.yml

Only during first run infrastructure.yml playbook will be save SSH access key to directory from infrastructure preferences
If you have problem with your playbook and create instances try flush ansible cache before run playbook or setup cache parameter to 0 in ec2.ini file


##Configure environment
Setup AWS security credentials with installed awscli
Use tool "aws configure" for setup parameters
AWS Access Key ID [AwsAccessKey]
AWS Secret Access Key [AwsSecretKey]
Default region name [eu-west-1] 
Default output format [None]: 


##Setup
For setup parameter use preferences.yml file
```
#####################
#  Preferences File #
#####################

# AWS Region
awsEc2region: eu-west-1
awsEc2availableZone: eu-west-1a

# VPC Datacenter Name
datecenterEc2name: Ireland

# VPC Cidr
vpcEc2cidr: 10.1.0.0/16

# VPC Subnet Cidr
vpcEc2subnetCidr: 10.1.0.0/28

# Instances Private IP
applicationEc2private: 10.1.0.11
databaseEc2private: 10.1.0.12

#Ec2 instance AMI image (current environment use Ubuntu 16.04)
amiEc2image: ami-6d48500b

# SSH Key Name
sshEc2key: IrelandSshKey

# Save Folder For SSH Key
sshEc2saveFolder: /Users/lukasbudisky

# Environment Type
envEc2: Production

# Ec2 Instance Type For Database Server
dbEc2instanceType: t2.micro

# Ec2 Instance Type For Application Server
appEc2instanceType: t2.micro
```


##Playbook tags:
infrastructure.yml
1. sshKeySetup: create key pair in datacenter and save to directory
2. createInfrastructure: create VPC, create application and database instances

serverApplication.yml
1. setupPython: setup python2.7 if is not install
2. gatherFacts: collect Ec2 facts
3. setupApplicationServer: configure application server
4. deployApplicationServer: deploy application

serverDatabase.yml
1. setupPython: setup python2.7 if is not install
2. gatherFacts: collect Ec2 facts
3. setupDatabaseServer: configure database server


##Usage Example:
--------------
Create VPC infrastructure a load instances:
```
ansible-playbook -i inventory/ec2.py infrastructure.yml --user ubuntu --private-key=/home/$USER/IrelandSshKey.pem
```

Setup Application Ec2 instance
```
ansible-playbook -i inventory/ec2.py serverApplication.yml --user ubuntu --private-key=/home/$USER/IrelandSshKey.pem
ansible-playbook -i inventory/ec2.py serverDatabase.yml --user ubuntu --private-key=/home/$USER/IrelandSshKey.pem
```

Deployment
```
ansible-playbook -i inventory/ec2.py serverApplication.yml --user ubuntu --private-key=/home/$USER/IrelandSshKey.pem
```

##Tips For Improvements:
If you have configured vpn in aws:
Change cidr for mysql,ssh security group to private cidr.
Change assing_public_ip to "no" in install database server.
Change ec2.ini preferences to use private dns and ip addresses

If you have route 53 in aws cloud:
Create automatization for DNS records and letsencrypt

Change passwords and remove Example passwords from git:
vault password
vault/users.yml file
preferences.yml

Instances
split partition for home, app, db with lvm
configure mail server for drupal

##Authors
dewist s.r.o
Lukas Budisky
Lukas Andrasovic
